# convention-skeleton-bundle

Каркас, для symfony bundle, с авто подключением конфигурационных файлов и расширений DI по соглашению.

## Соглашение

### Структура директррий и файлов конфигурации при использовании SkeletonBundle

    MyBundle.php
    Resources
        config
            parameters.yml
            services.yml

В этом случае при использовании

    use Keepper\ConventionSkeleton\SkeletonBundle;
    
    classMyBundle extends SkeletonBundle {
    }
    
Автоматически добавиться расширение симфони, которое 
подключит файлы конфигурации parameters.yml и services.yml

### Структура репозитория, при использования Тестового Окружения для изолированного тестирования symfony bundle'ов

    run/
    src/
        Resources/
            config/
                ...    // см. Выше
            test.yml   // Файл с базовыми настройками приложения
        MyBundle.php
    tests/
        MyCodeTest.php
    vendor/
    composer.json
    
В этом случае тесты для автоматического подънятия окружения симфони нужно писать так:

    use Keepper\ConventionSkeleton\Tests\Symfony\ServiceTestCase;
    
    class MyCodeTest extends ServiceTestCase {
    
        // Регистрация Бандлов в ядре
        function bundleClassesToRegister(array $addTo = []): array {
            return parent::bundleClassesToRegister([
                MyBundle::class,
                // Другие необходимые классы бандлов
            ]);
        }
        
        // Регистрация параметров
        function baseParameters(array $addTo = []): array {
            return parent::baseParameters([
               'myParam' => 'my value',
               // Другие необходимые параметры
           ]);
        }
        
        // Деприватизация сервисов
        function servicesToUnprivate(array $addTo = []): array {
            return parent::servicesToUnprivate([
               'myServiceId',
               // Другие сервисы
           ]);
        }
    }

## Инсталяция 

1. Добавьте репозиторий в свой composer.json

        "repositories": [
            ...
            {
              "type": "vcs",
              "url": "https://gitlab.com/keepper/convention-skeleton-bundle.git"
            }
        ]
        
1. Запустите в консоле

        composer require keepper/convention-skeleton-bundle


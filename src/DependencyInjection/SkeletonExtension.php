<?php
namespace Keepper\ConventionSkeleton\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;

class SkeletonExtension extends Extension {

	protected $configurationPath;
	protected $configurationFiles;
	protected $aliase;

	public function setConfiguration(
		string $configurationPath,
		array $configurationFiles = null
	) {
		$this->configurationPath = $configurationPath;
		$this->configurationFiles = $configurationFiles ?? ['parameters.yml', 'services.yml'];
	}

	public function setAliase(string $aliase) {
		$this->aliase = $aliase;
	}

	function getAlias() {
		if ( is_null($this->aliase) ) {
			return parent::getAlias();
		}
		return Container::underscore($this->aliase);
	}

	/**
	 * {@inheritdoc}
	 * @throws \Exception
	 */
	public function load(array $configs, ContainerBuilder $container) {
		$loader = new Loader\YamlFileLoader($container, new FileLocator($this->configurationPath));
		foreach ($this->configurationFiles as $filename) {
			$loader->load($filename);
		}
	}
}
<?php

namespace Keepper\ConventionSkeleton;

use Keepper\ConventionSkeleton\DependencyInjection\SkeletonExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SkeletonBundle extends Bundle {

	protected function getConfigurationPath(): string {
		return $this->getPath() . '/Resources/config';
	}

	protected function getConfigurationFiles(): ?array {
		return [
			'parameters.yml',
			'services.yml'
		];
	}

	/**
	 * @inheritdoc
	 */
	protected function createContainerExtension() {
		if (!class_exists($class = $this->getContainerExtensionClass())) {
			if ( $class == SkeletonExtension::class ) {
				return null;
			}
			$class = SkeletonExtension::class;
		}

		$extension = new $class();
		if ( $extension instanceof SkeletonExtension ) {
			$extension->setConfiguration(
				$this->getConfigurationPath(),
				$this->getConfigurationFiles()
			);

			$extension->setAliase(substr($this->getName(), 0, -6));
		}
		return $extension;
	}
}
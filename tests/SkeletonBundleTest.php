<?php
namespace Keepper\ConventionSkeleton\Tests;

use Keepper\ConventionSkeleton\DependencyInjection\SkeletonExtension;
use Keepper\ConventionSkeleton\Tests\Fixture\FixtureBundle;

class SkeletonBundleTest extends \PHPUnit_Framework_TestCase {
	public function testConfiguration() {
		$bundle = new FixtureBundle();

		$this->assertEquals(__DIR__ .'/Fixture/Resources/config', $bundle->getCfgPath());
		$this->assertEquals(['parameters.yml','services.yml'], $bundle->getCfgFiles());
	}

	public function testExtensionAliase() {
		$ext = new SkeletonExtension();
		$ext->setAliase('Fixture');
		$this->assertEquals('fixture', $ext->getAlias());
	}

	public function testExtension() {
		$bundle = new FixtureBundle();
		$extension = $bundle->getContainerExtension();
		$this->assertNotNull($extension, 'Expected extension instance of SkeletonExtension');
		$this->assertInstanceOf(SkeletonExtension::class, $extension);
	}
}
<?php
namespace Keepper\ConventionSkeleton\Tests\Fixture;

use Keepper\ConventionSkeleton\SkeletonBundle;

class FixtureBundle extends SkeletonBundle {
	public function getCfgPath() {
		return $this->getConfigurationPath();
	}

	public function getCfgFiles() {
		return $this->getConfigurationFiles();
	}
}
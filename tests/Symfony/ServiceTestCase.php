<?php
namespace Keepper\ConventionSkeleton\Tests\Symfony;

use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class ServiceTestCase extends KernelTestCase {

    private $namespace;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    static protected $pathSearchDeep = 2;
    static protected $postDir = '';
    static protected $debugSearchPath = false;

    static function setUpBeforeClass() {
        $logger = self::$debugSearchPath ? new Logger('test-env') : new NullLogger();
        $corePath = self::getRootPath($logger).'/run';
        if ( !file_exists($corePath) ) {
            $logger->error('Not exists run directory at path: '.$corePath);
            throw new \RuntimeException('Директория run не найдена по пути: '.$corePath);
        }
        $kernelDir = $_SERVER['KERNEL_DIR'] = realpath($corePath);
        $logger->debug('Kernel directory: '.$kernelDir);

        $cacheDirectory = realpath($kernelDir).'/cache/test';
        // Очищаем кэш
        exec(sprintf("rm -rf %s", escapeshellarg($cacheDirectory)));

        $possibleDirs = [
            realpath($kernelDir .'/../Resources/'),
            realpath($kernelDir .'/../src/'.self::$postDir.'/Resources/')
        ];

        $finded = false;
        foreach ($possibleDirs as $dir) {
            if ( empty($dir) ) {
                continue;
            }

            if ( !file_exists($dir) ) {
                $logger->debug('Try dir: '.$dir.' - not exists');
                continue;
            }
            UnitTestKernel::$configDir = $dir.'/';
            $finded = true;
            $logger->debug('Config directory is: '.$dir);
        }

        if ( !$finded ) {
            throw new \RuntimeException('Не смогли определить директорию с конфигурационными файлами. KernelDir: '.$kernelDir);
        }
    }

    static protected function getRootPath(LoggerInterface $logger = null) {
        $logger = $logger ?? new NullLogger();
        $pos = strrpos(static::class, '\\');
        $namespace = false === $pos ? '' : substr(static::class, 0, $pos);
        $names = explode('\\', $namespace);

        $logger->debug('Namespace: '.$namespace);
        while(true) {
            if (count($names) == 0) {
                throw new \RuntimeException('ConventionSkeleton: Имя теста, не подходит под соглашение. Нет возможности настроить тестовое окружение');
            }
            $name = array_pop($names);
            if ( 'Bundle' != substr($name, -6) ) {
                $logger->debug('Search bundle part: '.$name.' - skip');
                continue;
            }

            $path = implode('\\', $names).'\\'.$name;
            $className = $path.'\\'.$name;
            $logger->debug('Bundle: '.$className);

            if ( class_exists($className) ) {
                $reflected = new \ReflectionObject(new $className());

                $path = \dirname($reflected->getFileName());
                $logger->debug('Bundle filepath: '.$path);
                $postDir = '';
                $pathParts = explode('/', $path);
                $logger->debug('deep: '.static::$pathSearchDeep.', parts: '.print_r($pathParts, true));
                for($i=0; $i < static::$pathSearchDeep; $i++) {
                    $pathPart = array_pop($pathParts);

                    if ($pathPart != 'src') {
                        $logger->debug('path '.$pathPart.' != src');
                        if ($postDir == '') {
                            $postDir = $pathPart;
                        } else {
                            $postDir = $pathPart .'/'.$postDir;
                        }

                        continue;
                    }

                    self::$postDir = $postDir;
                    $result = implode('/', $pathParts);
                    $logger->debug('src dir: '.$result);
                    return $result;
                }
                $logger->error('src directory not find');
            } else {
                $logger->error('Bundle class: '.$className.' - not exists');
            }
        }

    }

    protected function setUp(){
        $this->logger = new Logger('SymfonyTestEnv');
        parent::setUp();
    }

    function bundleClassesToRegister(array $addTo= []): array {
        if (count($addTo) == 0) {
            return [];
        }
        return array_merge([], $addTo);
    }

    function servicesToUnprivate(array $addTo = []): array {
        if (count($addTo) == 0) {
            return [];
        }
        return array_merge([], $addTo);
    }

    function baseParameters(array $addTo = []): array {
        if (count($addTo) == 0) {
            return [];
        }
        return array_merge([], $addTo);
    }

    protected function getService(string $serviceName) {
        return static::$kernel->getContainer()->get($serviceName);
    }


}
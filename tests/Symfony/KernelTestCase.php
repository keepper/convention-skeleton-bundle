<?php
namespace Keepper\ConventionSkeleton\Tests\Symfony;

use \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase as SymfonyKernelTestCase;

abstract class KernelTestCase extends SymfonyKernelTestCase {

	protected static function getKernelClass() {
		return UnitTestKernel::class;
	}

	protected function setUp() {
		parent::setUp();

		/**
		 * @var UnitTestKernel $kernelClassName
		 */
		$kernelClassName = self::getKernelClass();
		// Setting base parameters
		$kernelClassName::setParameter('kernel.secret', 'Gdasd82$adGdasfwa#Yapuk6');
		foreach ($this->baseParameters() as $parameterName => $parameterValue) {
			$kernelClassName::setParameter($parameterName, $parameterValue);
		}

		/**
		 * @var UnitTestKernel static::$kernel
		 */
		static::$kernel = self::createKernel();

		// Setting bundle names for load
		foreach ($this->bundleClassesToRegister() as $bundleClassName) {
			static::$kernel->addBundle($bundleClassName);
		}

		// Setting service names for deprivate in test enviroments
		foreach ($this->servicesToUnprivate() as $serviceName) {
			static::$kernel->deprivate($serviceName);
		}

		static::$kernel->boot();
	}

	abstract function bundleClassesToRegister(): array;
	abstract function servicesToUnprivate(): array;
	abstract function baseParameters(): array;
}